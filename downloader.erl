-module(downloader).
-export([download/2, getUrl/3, getFileName/3, run/1]).

download(Url, Filename)->
    {ok, {{_, 200, _}, _, Body}} = httpc:request(get, {Url, []}, [], []),
    {ok, F} = file:open(Filename, [write,binary]),
    file:write(F, Body),
    file:close(F).

getUrl(Z, X, Y)->
    "http://c.tile.openstreetmap.org/" ++ integer_to_list(Z) ++ "/" ++ integer_to_list(X) ++ "/" ++ integer_to_list(Y) ++ ".png".

getFileName(Z, X, Y)->
	"./mapdata/" ++ integer_to_list(Y) ++ ".png".

myFun(N)->
    Url = getUrl(17, 1, N),
	Filename = getFileName (17, 1, N),
    download(Url, Filename).

run(N)->
    inets:start(),
    lists:map(fun(Num)->register(list_to_atom("pid" ++ integer_to_list(Num)), spawn(fun()->myFun(Num) end)) end, lists:seq( 0, N - 1)).
